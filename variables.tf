# Inputs

# Parameters within this module

variable "S3BucketName" {
  description = "S3 Bucket Name"
  default     = "bucketByMoeentest"
}
#variable "S3BucketVersioning" {
#  description = "S3 Bucket Versioning"
#}
#variable "S3BucketTag" {
#  description = "S3 Bucket Tag"
#}
#variable "Component" {
#	description = "Name for Component."
#}
#variable "Environment" {
#	description = "Name for Environment."
#}
#variable "backup" {
#	description = "Name for backup, true or false."
#}
#variable "LOB" {
#	description = "Name for Line Of business."
#}
#variable "Product" {
#	description = "Name for Product."
#}
#variable "Country" {
#	description = "Name for Country."
#}
#variable "EncryptionType" {
#  description = "Encryption Type"
#}
#variable "CreatePolicy" {
#  description = "Create Policy"
#}
#variable "IAMUserArn"{
#	description = "IAM User For Bucket Policy"
#}
